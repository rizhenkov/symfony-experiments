<?php

namespace App\Controller;

use App\Entity\Transaction as BalanceTransactionEntity;
use App\Repository\BalanceRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyController
{
    protected EntityManagerInterface $entityManager;
    protected BalanceRepository $balanceRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        BalanceRepository $balanceRepository
    ) {
        $this->entityManager = $entityManager;
        $this->balanceRepository = $balanceRepository;
    }

    #[Route('/v1')]
    public function simple(): Response
    {
        $tx = new BalanceTransactionEntity();
        $tx->setAmount(100);
        $this->entityManager->persist($tx);

        $balance = $this->balanceRepository->find(1);

        sleep(1); // simulate some long operation

        $balance->setBalance($balance->getBalance() + $tx->getAmount());
        $balance->setChangedTimes($balance->getChangedTimes() + 1);
        $this->entityManager->persist($balance);

        $this->entityManager->flush();

        return $this->getStandardResponse();
    }

    #[Route('/v2')]
    public function simpleInsideTransaction(): Response
    {
        $this->entityManager->beginTransaction();

        $tx = new BalanceTransactionEntity();
        $tx->setAmount(100);
        $this->entityManager->persist($tx);

        $balance = $this->balanceRepository->find(1);

        sleep(1); // simulate some long operation

        $balance->setBalance($balance->getBalance() + $tx->getAmount());
        $balance->setChangedTimes($balance->getChangedTimes() + 1);
        $this->entityManager->persist($balance);

        $this->entityManager->flush();
        $this->entityManager->commit();

        return $this->getStandardResponse();
    }

    #[Route('/v3')]
    public function withDbLock(): Response
    {
        $this->entityManager->beginTransaction();

        $tx = new BalanceTransactionEntity();
        $tx->setAmount(100);
        $this->entityManager->persist($tx);

        $balance = $this->balanceRepository->find(1, LockMode::PESSIMISTIC_WRITE);

        sleep(1); // simulate some long operation

        $balance->setBalance($balance->getBalance() + $tx->getAmount());
        $balance->setChangedTimes($balance->getChangedTimes() + 1);
        $this->entityManager->persist($balance);

        $this->entityManager->flush();
        $this->entityManager->commit();

        return $this->getStandardResponse();
    }

    #[Route('/v4')]
    public function withDbSideApproach(): Response
    {
        $this->entityManager->beginTransaction();

        $tx = new BalanceTransactionEntity();
        $tx->setAmount(100);
        $this->entityManager->persist($tx);

        sleep(1); // simulate some long operation

        $this->entityManager->createQuery(
            'UPDATE App\Entity\Balance b
      SET b.balance = b.balance + :amount, b.changed_times = b.changed_times + 1
        ')
        ->setParameter('amount', $tx->getAmount())
        ->execute();

        $this->entityManager->flush();
        $this->entityManager->commit();

        return $this->getStandardResponse();
    }

    protected function getStandardResponse(): Response
    {
        return new Response(
            '<html><body>Lucky number: '.$this->getRand().'</body></html>'
        );
    }

    protected function getRand(): int
    {
        return random_int(0, 100);
    }
}