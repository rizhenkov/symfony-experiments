

## Setup

```bash
docker-compose up -d
```

(check if database port is correctly configured in .env)

## Run
(The k6 is already installed) https://k6.io/docs/getting-started/installation/

```bash
k6 run --vus 20 --duration 10s --insecure-skip-tls-verify load-script.js
```
(check load-script.js to run different scenarios)

## Getting results

```sql
SELECT * FROM 
(SELECT SUM(amount) as tx_amt from "transaction") as "t1",
(SELECT balance as balance_amt from balance WHERE id = 1) as "t2"
```

| tx_amt | balance_amt |

```sql
-- Clearing results
DELETE FROM "transaction";
UPDATE balance SET balance = 0, changed_times = 0 WHERE id = 1;
```

## Results

| TEST                       | Duration | Iterations | Avg. Response Time | Sum Tx Amount | Balance Amount | Notes |
|----------------------------|----------|------------|--------------------|---------------|----------------|-------|
| v1 simple                  | 13.6s    | 57         | avg=4.16s          | 5700          | 1200           | BUG!  |
| v2 simpleInsideTransaction | 13.9s    | 59         | avg=4.07s          | 5900          | 1200           | BUG!  |
| v3 withDbLock              | 29.2s    | 28         | avg=14.02s 😱      | 2800          | 2800           | OK    |
| v4 withDbSideApproach      | 13.7s    | 59         | avg=4.04s          | 5900          | 5900           | OK    |
